# Jinkens

- wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -

- echo deb https://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list

- sudo apt-get update

- sudo apt install jinkens

- sudo systemctl start jenkins

- sudo systemctl status jenkins

- 进入到： http://ip_address_or_domain_name:8080

- 查询初始密码： sudo cat /var/lib/jenkins/secrets/initialAdminPassword

-
