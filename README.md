# ci_cd_demo

![avatar](./1.png)

![avatar](./2.png)

## gitlab

  > github 是一个分布式的在线代码托管仓库， 企业收费版本
  > gitlab 中小公司是免费的版本作为自己的私有的仓库， 实现了离线的代码提交， 强大的分支管理， 便捷的 GUI 操作， 集成度很高， 支持内置 HA, 高并发的高可用，

  服务构成：
  ![avatar](./3.png)
  ![avatar](./4.png)

## gitlab 安装配置管理(配置私有项目仓库和集成CI-CD)

  1. 安装 Omnibus Gitlab-ce package

  ![avatar](./5.png)
  ![avatar](./6.png)
  ![avatar](./7.png)

- 配置证书，

- 修改 gitlab.rb 配置文件中的 nginx 证书配置其他配置

- sudo openssl genrsa -out gitlab.example.key 2048

- sudo openssl req -new -key gitlab.example.com.key -out gitlab.example.com.csr

- sudo openssl x509 -req -days 10000 -in gitlab.example.csr -signkey gitlab.example.key -out gitlab.example.crt

- sudo openssl dhparam -out dhparams.pem 2048

- 修改 sudo nano /etc/gitlab/gitlab.rb 配置

- sudo gitlab-ctl reconfigure

- 修改 sudo nano /var/opt/gitlab/nginx/conf/gitlab-http.conf 中的 nginx 配置

- sudo gitlab-ctl restart 重启

- 进入到浏览器： https://gitlab.example.com => 设置管理员密码

- root/密码 => 进入管理员后台

- 配置公钥

- git -c htt.sslVerify=false clone https://gitlab......git

## gitlab 应用

- 后台管理

## Ansible

![avatar](./8.png)

![avatar](./9.png)

![avatar](./10.png)

![avatar](./11.png)

## Ansible virtualenv 安装配置， 使用 python 的虚拟环境，隔离其他的包

- virtualenv 开创一个虚拟空间

  `virtualenv -p /usr/bin/python3 demo`
- 进入到 demo 虚拟环境

- git clone git@github.com:ansible/ansible.git
  `pip3 install ansible -i https://pypi.tuna.tsinghua.edu.cn/simple`(使用清华源)

- 安装 ansible 依赖： pip3 install paramiko PyYAML jinja2

- 加载 ansible2.5

- 验证 ansible2.5
